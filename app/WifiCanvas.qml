/*
 * Copyright (C) 2015 - Michael Zanetti <michael.zanetti@ubuntu.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import WiFi 1.0
import Ubuntu.Components 1.3

Canvas {
    id: canvas
    anchors.fill: parent

    property var wifiModel: null

    property int topMargins: units.gu(1)
    property int leftMargins: units.gu(5)
    property int bottomMargins: band == Iwlist.WiFiBand24GHz ? units.gu(4) : units.gu(6)
    property int rightMargins: units.gu(1)
    property int contentWidth: canvas.width - leftMargins - rightMargins
    property int contentHeight: canvas.height - topMargins - bottomMargins

    property int pxPdb: contentHeight / 8;

    property int band: Iwlist.WiFiBand24GHz
    property bool extendedChannels: true
    property int slots: band == Iwlist.WiFiBand24GHz ? 19 : (extendedChannels ? 125 : 104)
    property real slotWidth: contentWidth / slots;

    property string selectedHwAddr: ""
    property bool scanning: true

    property var allChannels: band == Iwlist.WiFiBand24GHz ? twoChannels : extendedChannels ? fiveChannelsFull : fiveChannels
    property var twoChannels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    property var fiveChannelsFull: [7,8,9,11,12,16,34,36,38,40,42,44,46,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140,149,153,157,161,165,183,184,185,187,188,189,192,196]
    property var fiveChannels: [34,36,38,40,42,44,46,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140,149,153,157,161,165]

    onBandChanged: requestPaint()
    onSelectedHwAddrChanged: requestPaint();

    onPaint: {
        var ctx = canvas.getContext('2d');
        ctx.save();

        ctx.reset()

        ctx.translate(leftMargins, topMargins)

        ctx.globalAlpha = 1//canvas.alpha;
        //ctx.fillStyle = canvas.fillStyle;
        ctx.font = "" + units.gu(1.5) + "px Ubuntu";

        canvas.paintGrid(ctx)
        enumerate(ctx)
        paintWifis(ctx)
        ctx.restore();

    }

    function paintGrid(ctx) {
        ctx.strokeStyle = "white";
        ctx.lineWidth = units.dp(1);

        ctx.beginPath();
        ctx.rect(0, 0, contentWidth, contentHeight)
        ctx.stroke();
        ctx.closePath();

        // Horizontal lines
        for (var i = 1; i < 8; i++) {
            ctx.beginPath();
            ctx.lineWidth = units.dp(1);
            ctx.strokeStyle = Qt.darker("white")
            ctx.moveTo(0, i * pxPdb);
            ctx.lineTo(contentWidth, i * pxPdb)
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            var label = "-" + (i + 2) + "0"
            var textSize = ctx.measureText(label)
            ctx.strokeStyle = "white"
            ctx.fillStyle = "white"
            ctx.lineWidth = 1;
            ctx.text(label, -textSize.width - units.gu(1), i * pxPdb + units.gu(.5))
            ctx.stroke();
            ctx.fill()
            ctx.closePath()
        }
    }

    function enumerate(ctx) {
        // enumate x axis
        ctx.beginPath();
        ctx.globalAlpha = 1;
        ctx.strokeStyle = "white"
        ctx.fillStyle = "white"
        for (var i = 0; i < 200; i++) {
            if (channelToX(i) >= 0) {
                var label = +i
                var textSize = ctx.measureText(label)
                ctx.text(label, channelToX(i) - textSize.width / 2, channelToY(i))
            }
        }

        ctx.stroke();
        ctx.fill()
        ctx.closePath();
    }

    function channelToX(channel) {
        if (band == Iwlist.WiFiBand24GHz) {
            if (channel > 0 && channel < 14) {
                return (channel + 1) * slotWidth;
            }
            if (channel == 14) {
                return 17 * slotWidth;
            }
            return -1;
        }
        if (band == Iwlist.WiFiBand5GHz) {
            var listIndex = allChannels.indexOf(channel);
            if (listIndex == -1) {
                return -1;
            }

            var slotIndex = channel;
            if (channel >= 183) {
                slotIndex -= 12
            }

            if (channel >= 149) {
                slotIndex -= (149 - 146);
            }
            if (channel >= 100) {
                slotIndex -= (100 - 70);
            }
            if (channel >= 34) {
                slotIndex -= 14;
            }
            if (channel >= 16) {
                slotIndex -= 2;
            }

            if (canvas.extendedChannels) {
                slotIndex -= 7;
            } else {
                slotIndex -= 16;
            }

            return slotIndex * slotWidth

        }
        return -1;
    }
    function channelToY(channel) {
        if (band == Iwlist.WiFiBand24GHz) {
            return contentHeight + units.gu(2)
        }

        var listIndex = allChannels.indexOf(channel);
        if (listIndex == -1) {
            return -1;
        }
        return contentHeight + units.gu(2 + (listIndex % 3) * 1.5)
    }

    function paintWifis(ctx) {
        var lColors = new Array();

        // Paint the wifis
        for (var i = 0; i < wifiModel.count; i++) {
            // If we have an index, only paint that one
            var item = wifiModel.get(i);
            if (canvas.selectedHwAddr != "" && item.hwAddr != canvas.selectedHwAddr) {
                print("selectedHwAddr is", canvas.selectedHwAddr, "Not painting", item.hwAddr)
                continue;
            }


            if (canvas.band != item.band) {
                print("Not for this band", canvas.band, item.band)
                continue;
            }


            var curveTopX = channelToX(item.channel)
            if (curveTopX == -1) {
                print("cureveTopX is -1. not painting wifi on channel", item.channel)
                continue;
            }

            var curveStartX = curveTopX - 2.1 * slotWidth;
            var curveEndX = curveTopX + 2.1 * slotWidth;

            var offset = (-item.level - 20) / 10 * pxPdb*2;
            var curveTopY = -contentHeight + offset

            if (lColors.length == 0) {
                lColors = app.colors.slice();
            }

            //var random = Math.floor(Math.random() * 100) % lColors.length
            //var color = lColors[random]
            //lColors.splice(random, 1)
            var color = app.colors[item.color % app.colors.length]

            paintCurve(ctx, curveStartX, curveTopX, curveEndX, curveTopY, color, 1);

            if (band == Iwlist.WiFiBand5GHz) {
                curveStartX -= 2*slotWidth;
                curveEndX += 2*slotWidth;
                paintCurve(ctx, curveStartX, curveTopX, curveEndX, curveTopY, color, 0.5);
            }

            ctx.beginPath();
            offset = (-item.level - 20) / 10 * pxPdb;
            ctx.globalAlpha = 1
            ctx.lineWidth = 1
            ctx.text(item.name, curveTopX, offset - units.gu(1))
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
        }
    }

    function paintCurve(ctx, curveStartX, curveTopX, curveEndX, curveTopY, color, opacity) {
        ctx.beginPath();

        ctx.globalAlpha = opacity
        ctx.strokeStyle = color
        ctx.lineWidth = units.dp(2)
        ctx.moveTo(curveStartX, contentHeight)
        ctx.quadraticCurveTo(curveTopX, curveTopY, curveEndX, contentHeight)
        ctx.stroke()
        ctx.closePath();

        ctx.beginPath();
        ctx.moveTo(curveStartX, contentHeight)
        ctx.quadraticCurveTo(curveTopX, curveTopY, curveEndX, contentHeight)
        ctx.fillStyle = color
        ctx.globalAlpha = opacity * 0.2
        ctx.fill()
        ctx.closePath();

    }

    property real radarPercent: 0;
    property int radarWidth: units.gu(10)

    Item {
        anchors {
            fill: parent
            leftMargin: canvas.leftMargins
            topMargin: canvas.topMargins
            rightMargin: canvas.rightMargins
            bottomMargin: canvas.bottomMargins
        }
        clip: true
        opacity: canvas.scanning ? 1 : 0
        Behavior on opacity {
            UbuntuNumberAnimation {}
        }

        Item {
            id: radar
            anchors { top: parent.top; bottom: parent.bottom; }
            width: radarWidth
            property int radarDistance: contentWidth

            property bool increasing: true
            Rectangle {
                height: parent.width
                width: parent.height
                anchors.centerIn: parent
                rotation: radar.increasing ? -90 : 90
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "transparent" }
                    GradientStop { position: 1.0; color: "green" }
                }
            }


            SequentialAnimation {
                running: canvas.scanning
                loops: Animation.Infinite

                ScriptAction { script: radar.increasing = true; }
                NumberAnimation {
                    target: radar
                    property: "x"
                    from: -radarWidth
                    to: radar.radarDistance
                    duration: 1000
                }
                ScriptAction { script: radar.increasing = false; }
                NumberAnimation {
                    target: radar
                    property: "x"
                    from: radar.radarDistance
                    to: -radarWidth
                    duration: 1000
                }
            }

        }
    }

}
